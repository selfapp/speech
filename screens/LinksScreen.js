import React from 'react';
import { FlatList, View, Text, SafeAreaView, TouchableOpacity, Share } from 'react-native';
import * as Permissions from 'expo-permissions';
import * as Contacts from 'expo-contacts';


export default class LinksScreen extends React.Component {
  
  constructor(props){
    super(props)
    this.state = {
      contactList: []
    }
  }
  componentDidMount(){
    this.showFirstContactAsync()
  }

  async showFirstContactAsync() {
    // Ask for permission to query contacts.
    const permission = await Permissions.askAsync(Permissions.CONTACTS);
    
    if (permission.status !== 'granted') {
      // Permission was denied...
      return;
    }
    const contacts = await Contacts.getContactsAsync({
      fields: [
        Contacts.PHONE_NUMBERS,
        Contacts.EMAILS,
      ]
    });
    if (contacts.total > 0) {
      // console.log(JSON.stringify(contacts))
      this.setState({ contactList: contacts.data })
    }
  }

  onShare = async () => {
    try {
      const result = await Share.share({
        message: 'You must be so proud of yourself, share this app with your friends to give them happiness',
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };


  renderItem = ({ item }) => {
    return (
      <View style={{ paddingVertical: 2, }}>
        <View style={{ flex: 1, borderRadius: 1, backgroundColor: '#FFF', paddingVertical: 10, shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.18, shadowRadius: 1, shadowColor: '#451B2D', elevation: 3 }}        >
          <View style={{ flex: 1, flexDirection: 'row', }}>

            <View style={{ flex: 1, marginLeft: 10, justifyContent: 'center' }}>
              <Text style={{ fontSize: 18 }}>{item.name}</Text>
            </View>
            <View>
              <TouchableOpacity style={{ padding: 5, marginRight: 5, borderRadius: 5, backgroundColor:'gray'}}
              onPress={()=>this.onShare()}
              >
                <Text>Invite</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    )
  }

  render() {
    return (
      <View style={{flex: 1 }}>
        <SafeAreaView style={{ flex: 1 }}>
          <FlatList
            data={this.state.contactList}
            keyExtractor={(item, index) => index.toString()}
            style={{ flex: 1 }}
            renderItem={this.renderItem}
          />
        </SafeAreaView>
      </View>
    );
  }
}

LinksScreen.navigationOptions = {
  title: 'Social',
  headerStyle: {
    backgroundColor: '#ff6417',
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
};


