import * as WebBrowser from 'expo-web-browser';
import React from 'react';
import {
  Image,
  ScrollView,
  Text,
  TouchableOpacity,
  FlatList,
  View,
  SafeAreaView,
  TouchableHighlight,
  Dimensions,
  Button
} from 'react-native';
import * as Speech from 'expo-speech';
import { MonoText } from '../components/StyledText';
import ModalDropdown from 'react-native-modal-dropdown';

const BASE_URL = 'http://suraj-test.iwoodapple.com/media/';

const { height, width } = Dimensions.get('screen');

export default class HomeScreen extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      listData: [],
      itemList:[],
      categoryList: [],
      selectedList:[]
    }
  }

  componentDidMount() {
    this.getData();
    this.getCategoryList();
  }



  async getData() {
    try {
      const response = await fetch(`http://suraj-test.iwoodapple.com/api/Speech/GetSpeech`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "apikey": 'YqOkZORykr'
        }
      });
      const responseJson = await response.json();
       console.log(JSON.stringify(responseJson))
      this.setState({ listData: responseJson, itemList: responseJson })
    } catch (e) {
      console.log(e)
    }
  }
  async getCategoryList() {
    try {
      const response = await fetch(`http://suraj-test.iwoodapple.com/api/Speech/GetCategory`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "apikey": 'YqOkZORykr'
        }
      });
      const responseJson = await response.json();
       console.log(JSON.stringify(responseJson))
      this.setState({ categoryList: responseJson })
    } catch (e) {
      console.log(e)
    }
  }

  playSelectedList(){
    this.state.selectedList.map((item) => {
      Speech.speak(item.speech)
    })
  }

  searchFilterFunction = text => {    
    const newData = this.state.listData.filter(item => {      
      const itemData = `${item.CategoryID.toUpperCase()}   
      ${item.CategoryID.toUpperCase()} ${item.CategoryID.toUpperCase()}`;
       const textData = text.toUpperCase();
       return itemData.indexOf(textData) > -1;    
    });
    
    this.setState({ itemList: newData });  
  };
  

  renderItem = ({ item }) => {
    return (
      <View  style={{ flex: 1, margin: 5,padding:5, backgroundColor: '#fff',borderWidth:1,borderColor:'#ccc', height: 120,justifyContent:'center',alignItems:'center'}}>
        <TouchableOpacity 
          onPress={()=> {
            this.setState({ selectedList: [...this.state.selectedList, item]})
            Speech.speak(item.speech)
          }}
        >
     <View>
              <Image
                source={{uri: `${BASE_URL}${item.image}`}}
                style={{ marginRight: 5, height: 80, width: 80,justifyContent:'center' }}
                resizeMode={'contain'}
              />
               <Text style={{flex: 1,textAlign: 'center', textTransform:'capitalize', fontSize: 14 ,justifyContent:'center',alignItems:'center'}}>{item.name}</Text>
            </View>
        </TouchableOpacity>
      </View>
    )
  }
  dropdownrenderButtonText(rowData) {
    const {name, id} = rowData;
    this.searchFilterFunction(`${id}`)
    return `${name}`;
  }


  dropdownrenderRow(rowData, rowID, highlighted) {
    return (
      <TouchableHighlight style={{ width: width-40, paddingHorizontal: 10, alignItems:'center' }} underlayColor='cornflowerblue'>
          <View style={{ flexDirection: 'row', justifyContent:'space-between', alignItems:'center'}}>
          <Image style={{height: 50, width: 50}}
                 mode='stretch'
                 source={{uri: rowData.image}}
          />
          <Text style={[ highlighted && {color: 'mediumaquamarine'}]}>
            {`${rowData.name}`}
          </Text>
          </View>
      </TouchableHighlight>
    );
  }



  render() {

    return (
     <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{ flex: 1 ,paddingLeft:10,paddingRight:10}}>
        <SafeAreaView style={{ flex: 1 }}>
          <View style={{ flex: 1, marginTop: 10, height: 40, paddingHorizontal: 5, justifyContent:'center', borderRadius: 5, borderWidth: 1, borderColor: 'gray'}}>
              <ModalDropdown options={this.state.categoryList}
              defaultValue={'Select category'}
              style={{ width: 300}}
              renderButtonText={(rowData) => this.dropdownrenderButtonText(rowData)}
              renderRow={this.dropdownrenderRow.bind(this)}
              />
        </View>
        <View style={{ flex: 1}}>
          <FlatList
          numColumns={3}
            data={this.state.itemList}
            keyExtractor={(item, index) => index.toString()}
            showsHorizontalScrollIndicator={false}
            renderItem={this.renderItem}
          />
          </View>
          <View style={{ height: 80, flexDirection: 'row'}}>
          <View style={{ flex: 4}}>
          <FlatList
            horizontal={true}
            data={this.state.selectedList}
            keyExtractor={(item, index) => index.toString()}
            showsHorizontalScrollIndicator={false}
            renderItem={this.renderItem}
          />
          </View>
          <View style={{ flex: 1 }}>
            <Button
            title={'Play'}
            style={{ height: 40}}
            onPress={()=> this.playSelectedList()}
            />
            <Button
            title={'clear'}
            style={{ height: 40}}
            onPress={()=> this.setState({ selectedList: []})}
            />
          </View>
          </View>
        </SafeAreaView>
      </View>
     </ScrollView>
    );
  }
}



HomeScreen.navigationOptions = {
  title: 'We Can Express',
  headerStyle: {
    backgroundColor: '#ff6417',
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  }
};