import React from 'react';
import { Text, View,Button, SafeAreaView, TextInput } from 'react-native';
import { ExpoConfigView } from '@expo/samples';

export default class AddCategory extends React.Component {
  
  constructor(props) {
    super(props)
    this.state={
        categoryName:''
    }
  }


  async saveCategory() {
    try {
      const response = await fetch(`http://suraj-test.iwoodapple.com/api/Speech/AddCategory`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "apikey": 'YqOkZORykr'
        },
        body:{
            "name": this.state.categoryName,
	        "image":"test.jpg"
        }
      });
      const responseJson = await response.json();
       console.log(JSON.stringify(responseJson))
    } catch (e) {
      console.log(e)
    }
  }

  render(){
  return (
    <View style={{ flex: 1 }}>
        <SafeAreaView style={{ flex: 1 }}>
            <View style={{ marginVertical: 20}}>
            <Text>Category name</Text>
            <TextInput
                placeholder={'Enter category name'}
                onChange={(text)=> this.setState({categoryName: text})}
            />

            <Button
                title={'Save'}
            />

            </View>
        </SafeAreaView>
    </View>
  )
  }
}

AddCategory.navigationOptions = {
  title: 'Add Category',
  headerStyle: {
    backgroundColor: '#ff6417',
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
};
