import React from 'react';
import { Text, View,Button, Dimensions, Image, SafeAreaView, TextInput, TouchableHighlight } from 'react-native';
import { ExpoConfigView } from '@expo/samples';
import ModalDropdown from 'react-native-modal-dropdown';

const { height, width } = Dimensions.get('screen');

export default class AddSpeech extends React.Component {
  
  constructor(props) {
    super(props)
    this.state={
        name:'',
        speechText: '',
        selectedCategory:'',
        categoryList:[]
    }
  }
componentDidMount(){
    this.getCategoryList()
}

  async getCategoryList() {
    try {
      const response = await fetch(`http://suraj-test.iwoodapple.com/api/Speech/GetCategory`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "apikey": 'YqOkZORykr'
        }
      });
      const responseJson = await response.json();
       console.log(JSON.stringify(responseJson))
      this.setState({ categoryList: responseJson })
    } catch (e) {
      console.log(e)
    }
  }

  async saveCategory() {
    try {
      const response = await fetch(`http://localhost:64746/api/Speech/AddSpeech`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "apikey": 'YqOkZORykr'
        },
        body:{
            "name":this.state.name,
            "image":"test.jpg",
            "speech":this.setState.speechText,
            "CategoryID":this.state.selectedCategory

        }
      });
      const responseJson = await response.json();
       console.log(JSON.stringify(responseJson))
    } catch (e) {
      console.log(e)
    }
  }

  dropdownrenderButtonText(rowData) {
    const {name, id} = rowData;
    this.setState({selectedCategory: id})
    return `${name}`;
  }


  dropdownrenderRow(rowData, rowID, highlighted) {
    return (
      <TouchableHighlight style={{ width: width-40, paddingHorizontal: 10, alignItems:'center' }} underlayColor='cornflowerblue'>
          <View style={{ flexDirection: 'row', justifyContent:'space-between', alignItems:'center'}}>
          <Image style={{height: 50, width: 50}}
                 mode='stretch'
                 source={{uri: rowData.image}}
          />
          <Text style={[ highlighted && {color: 'mediumaquamarine'}]}>
            {`${rowData.name}`}
          </Text>
          </View>
      </TouchableHighlight>
    );
  }



  render(){
  return (
    <View style={{ flex: 1 }}>
        <SafeAreaView style={{ flex: 1 }}>
        <View style={{ marginTop: 10, marginHorizontal: 15, height: 40, paddingHorizontal: 5, justifyContent:'center', borderRadius: 5, borderWidth: 1, borderColor: 'gray'}}>
              <ModalDropdown options={this.state.categoryList}
              defaultValue={'Select category'}
              style={{ width: 300}}
              renderButtonText={(rowData) => this.dropdownrenderButtonText(rowData)}
              renderRow={this.dropdownrenderRow.bind(this)}
              />
        </View>
<View style={{ flex: 1, marginHorizontal: 15}}>
            <View style={{ marginVertical: 20}}>
            <Text>Name</Text>
            <TextInput
                placeholder={'Enter name'}
                onChange={(text)=> this.setState({name: text})}
            />

            <Text style={{ marginTop: 15}}>Speech</Text>
            <TextInput
                placeholder={'Enter speech'}
                onChange={(text)=> this.setState({speechText: text})}
            />

            <Button
                title={'Save'}
                onPress={()=> this.saveCategory()}
            />
</View>
            </View>
        </SafeAreaView>
    </View>
  )
  }
}

AddSpeech.navigationOptions = {
  title: 'Add Speech',
  headerStyle: {
    backgroundColor: '#ff6417',
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
};
