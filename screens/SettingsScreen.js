import React from 'react';
import { Text, View,Image } from 'react-native';
import { ExpoConfigView } from '@expo/samples';

export default class SettingsScreen extends React.Component {
  
  constructor(props) {
    super(props)
  }
  render(){
  return (
    <View style={{ flex: 1, alignItems: 'center'}}>
        <Image
          style={{width: 250, height: 250,justifyContent:'center',alignItems:'center'}}
          source={require('../assets/images/coming-soon.jpg')}
        />
    </View>
  )
  }
}

SettingsScreen.navigationOptions = {
  title: 'Reminder',
  headerStyle: {
    backgroundColor: '#ff6417',
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
};
